;;;
;;; Coerce user-full-name to lowercase.
;;;

(defadvice user-full-name (after downcase-name)
  "Downcase results of user-full-name."
  (setq ad-return-value (downcase ad-return-value)))
(ad-activate 'user-full-name)


;;;
;;; Compilation.
;;;
(require 'compile)

;; SRT can construct pathnames containing doubled slashes //.
;; however, when emacs sees a double slash, it goes back to the root.
;; so filter those out.
(defadvice compilation-find-file (before doubleslash-fix 'activate)
  "Filter out double slashes in the input to this function."
  (let ((filename (ad-get-arg 1)))
    (while (string-match "//" filename)
      (setq filename (concat (substring filename 0 (1+ (match-beginning 0)))
                             (substring filename (match-end 0)))))
    (ad-set-arg 1 filename)))


;;;
;;; work around ispell bug in 24.3; `A' gives an error in TeX buffers.
;;; fixed in 24.4?  
;;; http://lists.gnu.org/archive/html/emacs-diffs/2013-04/msg00116.html
;;;
(defadvice comment-padright (before comment-padright-norm 'activate)
  (comment-normalize-vars))
(ad-activate 'comment-padright)

(defadvice ispell-send-string (before ispell-kill-quotes activate)
  (setq string (replace-regexp-in-string "''\\([ ,.;:!(){}/\\?]\\|$\\)" "  \\1" string))
  (setq string (replace-regexp-in-string "'\\([ ,.;:!(){}/\\?]\\|$\\)" " \\1" string))
  )
(ad-activate 'ispell-send-string)


;; (when (require 'semantic nil t)
;;   (defun semanticdb-save-all-db-idle ()
;;     "Save all semantic tag databases from idle time.
;;  Exit the save between databases if there is user input."
;;     (semantic-safe "Auto-DB Save: %S"
;;                    (semantic-exit-on-input 'semanticdb-idle-save
;;                                            (save-excursion
;;                                              (mapc (lambda (db)
;;                                                      (semantic-throw-on-input 'semanticdb-idle-save)
;;                                                      (semanticdb-save-db db t))
;;                                                    semanticdb-database-list))
;;                                            ))
;;     )
;; )



(when (< emacs-major-version 25)
  (defun directory-name-p (s)
    (string= (substring s -1) "/"))

  (defun directory-files-recursively (dir regexp &optional include-directories)
    "Return list of all files under DIR that have file names matching REGEXP.
This function works recursively.  Files are returned in \"depth first\"
order, and files from each directory are sorted in alphabetical order.
Each file name appears in the returned list in its absolute form.
Optional argument INCLUDE-DIRECTORIES non-nil means also include in the
output directories whose names match REGEXP."
    (let ((result nil)
          (files nil)
          ;; When DIR is "/", remote file names like "/method:" could
          ;; also be offered.  We shall suppress them.
          (tramp-mode (and tramp-mode (file-remote-p (expand-file-name dir)))))
      (dolist (file (sort (file-name-all-completions "" dir)
                          'string<))
        (unless (member file '("./" "../"))
          (if (directory-name-p file)
              (let* ((leaf (substring file 0 (1- (length file))))
                     (full-file (expand-file-name leaf dir)))
                ;; Don't follow symlinks to other directories.
                (unless (file-symlink-p full-file)
                  (setq result
                        (nconc result (directory-files-recursively
                                       full-file regexp include-directories))))
                (when (and include-directories
                           (string-match regexp leaf))
                  (setq result (nconc result (list full-file)))))
            (when (string-match regexp file)
              (push (expand-file-name file dir) files)))))
      (nconc result (nreverse files))))
  
  )


(require 'copyright)
(defun copyright-update-year (replace noquery)
  ;; This uses the match-data from copyright-find-copyright/end.
  (goto-char (match-end 1))
  (copyright-find-end)
  (setq copyright-current-year (format-time-string "%Y"))
  (unless (string= (buffer-substring (- (match-end 3) 2) (match-end 3))
                   (substring copyright-current-year -2))
    (if (or noquery
            (save-window-excursion
              (switch-to-buffer (current-buffer))
              ;; Fixes some point-moving oddness (bug#2209).
              (save-excursion
                (y-or-n-p (if replace
                              (concat "Replace copyright year(s) by "
                                      copyright-current-year "? ")
                            (concat "Add " copyright-current-year
                                    " to copyright? "))))))
        (if replace
            (replace-match copyright-current-year t t nil 3)
          (let ((size (save-excursion (skip-chars-backward "0-9"))))
            ;; sss change == 1 to < 10
            (if (and (< (% (- (string-to-number copyright-current-year)
                               (string-to-number (buffer-substring
                                                  (+ (point) size)
                                                  (point))))
                            100)
                         10)
                     (or (eq (char-after (+ (point) size -1)) ?-)
                         (eq (char-after (+ (point) size -2)) ?-)))
                ;; This is a range so just replace the end part.
                (delete-char size)
              ;; Insert a comma with the preferred number of spaces.
              (insert
               (save-excursion
                 (if (re-search-backward "[0-9]\\( *, *\\)[0-9]"
                                         (line-beginning-position) t)
                     (match-string 1)
                   ", ")))
              ;; If people use the '91 '92 '93 scheme, do that as well.
              (if (eq (char-after (+ (point) size -3)) ?')
                  (insert ?')))
            ;; Finally insert the new year.
            (insert (substring copyright-current-year size)))))))
