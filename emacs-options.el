;;;
;;; General editing options.
;;;

(put 'eval-expression 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(setq inhibit-startup-message t)
(setq-default indent-tabs-mode nil)
(setq find-file-existing-other-name t)
(setq initial-scratch-message "")
(setq visible-bell t)
(if (fboundp 'blink-cursor-mode)
    (blink-cursor-mode 0))
(setq display-time-24hr-format t)
(setq line-move-visual nil)
(setq crypt-freeze-vs-fortran nil) ; .F files are fortran, not `frozen' files.
(setq eval-expression-print-length 100)
(setq diff-switches "-u")
(setq smiley-regexp-alist nil)
;; xemacs only; really should only be set to nil for remote displays.
(setq x-selection-strict-motif-ownership nil)
(if (fboundp 'transient-mark-mode)
    (transient-mark-mode t))

;;;
;;; completion
;;;
;(if (string= (substring (emacs-version) 0 6) "XEmacs")
;    (load "completer")
;  (load "complete")
;  (setq PC-meta-flag t))



;;;
;;; display options.
;;;
;; turn off toolbar, gutters
(if (string= (substring (emacs-version) 0 6) "XEmacs")
    (set-specifier default-toolbar-visible-p nil))  ; turn off toolbar
(setq gutter-buffers-tab-visible-p nil)
(setq gutter-buffers-tab-enabled nil)
(modify-all-frames-parameters '((tool-bar-lines . nil)))


;; emacs's default record format for new files is varying, with a maxrecl
;; of 512.  some packages, however, expect to be able to write arbitrarily
;; long lines.  so use stream_lf format as the default.
(setq vms-stmlf-recfm t)


;;;
;;; dired
;;;
(setq dired-mode-hook '(lambda nil (setq truncate-lines t)))

;;;
;;; change-log
;;;
(setq change-log-mode-hook '(lambda nil (auto-fill-mode)))

;;;
;;; yow
;;;
(setq yow-file (expand-file-name "~/elisp/yow.lines"))


;;;
;;; gopher
;;;
(setq gopher-support-bookmarks t)


;;;
;;; bbdb
;;;
(when (maybe-require 'bbdb)
  (bbdb-initialize 'mh-e 'gnus 'sendmail 'message 'sc)
  (setq vmsmail-Subject-mode-hook '(lambda () (require 'bbdb-vmsmail))))


;;;
;;; appt
;;;
(setq list-diary-entries-hook
      (list 'appt-make-list))


;;;
;;; gnus
;;;
(setq gnus-startup-file "~/news/.newsrc")
(setq gnus-default-article-saver 'gnus-summary-save-in-file)
(setq gnus-article-save-directory "~/news")
(setq gnus-novice-user nil)
(setq gnus-local-organization "ha!")
(setq gnus-mail-reply-method 'gnus-mail-reply-using-mhe)
(setq gnus-mail-forward-method 'gnus-mail-forward-using-mhe)
(setq gnus-mail-other-window-method 'gnus-mail-other-window-using-mhe)
(setq nnmail-spool-file nil)
(setq nnmail-delete-incoming nil)
(setq nnmh-get-new-mail nil)


;;;
;;; auctex
;;;
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq TeX-insert-braces nil)


;;;
;;; planner
;;;
(setq planner-project "planner")
(setq muse-project-alist
      '(("planner"
         ("~/Plans"
          :default "TaskPool"
          :major-mode planner-mode
          :visit-link planner-visit-link)
         (:base "planner-xhtml"
                :path "~/Plans/html"))))


;;;
;;; ispell
;;;
;(nconc (car ispell-tex-skip-alists)
;       '(("\\\\chinese" ispell-tex-arg-end)))



;;;
;;; supercite
;;;
(add-hook 'mail-citation-hook 'sc-cite-original)


;;;
;;; mh-e
;;;
(setq mh-graphical-smileys-flag nil)
