;;;
;;; Configure font-locking to use font changes rather than colors.
;;; There are several different ways this needs to be done,
;;; depending on the emacs version.
;;;

;; Use lazy locking.  Pick lazy-shot if it's available; otherwise,
;; use lazy-lock.
(cond
 ((fboundp 'turn-on-lazy-shot)
  (add-hook 'font-lock-mode-hook 'turn-on-lazy-shot))
 ((fboundp 'turn-on-lazy-lock)
  (add-hook 'font-lock-mode-hook 'turn-on-lazy-lock)))

;; Turn on syntax highlighting.  Use fonts, not colors.
(setq try-oblique-before-italic-fonts t)
(setq *try-oblique-before-italic-fonts* t)
(setq font-lock-use-fonts '(x))
(setq font-lock-use-colors nil)
(require 'font-lock)
(if (featurep 'font-latex)
    (require 'font-latex))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; For xemacs 21+

;; *sigh*  That seems not to work with xemacs 21...
(defun sss-set-facespec (face spec)
  (put face 'face-defface-spec spec)
  (face-display-set face spec))
  

(defun sss-remove-foreground (spec)
  (plist-remprop spec ':foreground))

(defun sss-remove-color-specs (spec)
  (remove-if (lambda (s) (and (listp (car s)) (equal (caar s) '(class color))))
             spec))


(defun sss-add-color-to-mono (spec)
  (mapcar (lambda (s) (if (and (listp (car s))
                               (eq (caaar s) 'class)
                               (memq 'grayscale (caar s)))
                          (list (list (append (caar s) '(color)) (cadar s)) 
                                (sss-remove-foreground (cadr s)))
                        s)) spec))

(defun sss-make-mono-spec (spec)
  (sss-add-color-to-mono (sss-remove-color-specs spec)))

(defun sss-make-mono-face (face)
  (sss-set-facespec face (sss-make-mono-spec (get face 'face-defface-spec)))
  (set-face-property face 'foreground
                     (face-property 'default 'foreground))
  )

(defun sss-unbolden1 (s)
  (plist-put s ':bold nil))

(defun sss-unbolden-spec (spec)
  (mapcar (lambda (s)
            (list (car s) (sss-unbolden1 (cadr s))))
          spec))

(defun sss-unbolden-face (face)
  (sss-set-facespec face (sss-unbolden-spec (get face 'face-defface-spec))))

(if (and (string= (substring (emacs-version) 0 6) "XEmacs")
         (emacs-version>= 21 1))
    (progn
      (mapcar 
       (lambda (face)
         (let ((name (symbol-name face)))
           (if (and (>= (length name) 10)
                    (string= (substring name 0 10) "font-lock-"))
               (sss-make-mono-face face))))
       (face-list))

      (sss-unbolden-face 'font-lock-comment-face)
      (sss-set-facespec 'font-lock-doc-string-face '((t (:italic t :bold nil))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; For emacs 21+
;;


(defun emacs-set-font-lock-faces (&optional frame)
  (mapcar 
   (lambda (face)
     (let ((name (symbol-name face)))
       (when (and (>= (length name) 10)
                  (string= (substring name 0 10) "font-lock-"))
         (set-face-foreground face 'unspecified))))
   (face-list))

  (set-face-attribute font-lock-comment-face       nil     :slant 'italic)
  (set-face-attribute font-lock-doc-face           nil     :slant 'italic)
  (set-face-attribute font-lock-string-face        nil     :slant 'italic)
  (set-face-attribute font-lock-keyword-face       nil     :weight 'bold)
  (set-face-attribute font-lock-builtin-face       nil     :underline t)
  (set-face-attribute font-lock-function-name-face nil     :underline t)
  (if (facep 'font-lock-preprocessor-face)
      (set-face-attribute font-lock-preprocessor-face  nil    :slant 'italic
                                                              :weight 'bold))
  (set-face-attribute font-lock-warning-face       nil     :box t)

  ;; font-lock-variable-name-face
  ;; font-lock-type-face
  ;; font-lock-constant-face
  ;; font-lock-comment-delimeter-face
  ;; font-lock-negation-char-face
  )


(when (string= (substring (emacs-version) 0 12) "GNU Emacs 21")
  ;; Isn't on by default in this version.
  (global-font-lock-mode t)

  ;; Have to do this in emacs 21 to ensure that colors stay off...
  (mapcar 
   (lambda (face)
     (let ((name (symbol-name face)))
       (when (and (>= (length name) 10)
                  (string= (substring name 0 10) "font-lock-"))
         (custom-set-faces (list face
                                 '((t (:foreground "unspecified")))))
         )))
   (face-list))

  (setq default-frame-alist 
        (cons '(font . "-*-nimbus mono l-medium-r-normal--*-*-*-*-*-*-*-*")
              default-frame-alist))


  ;; others that look decent
  ; -*-courier-medium-r-normal--14-*-*-*-m-*-*-*
  ; -efont-fixed-medium-r-normal--16-160-75-75-c-160-iso10646-1
  ; -sony-fixed-medium-r-normal--16-120-100-100-c-80-iso8859-2
  )


(when (string= (substring (emacs-version) 0 9) "GNU Emacs")
  (emacs-set-font-lock-faces))


(custom-set-faces
 '(default ((t (:family "DejaVu Sans Mono")))))
(set-face-attribute 'variable-pitch nil ':family "DejaVu Sans")
