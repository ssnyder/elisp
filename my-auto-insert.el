;; customizations for auto-insert.


(defun read-autoinsert-symbols ()
  (if (file-readable-p "AUTOINSERT")
      (save-excursion
        (set-buffer (find-file-noselect "AUTOINSERT"))
        (let (syms)
          (goto-char (point-min))
          (while (< (point) (point-max))
            (if (looking-at "\\(.*\\) *: *\\(.*\\)")
                (setq syms (cons
                            (cons (match-string 1) (match-string 2))
                            syms)))
            (forward-line 1))
          syms))))


(defun autoinsert-symbol (sym)
  (cdr (assoc sym (read-autoinsert-symbols))))


(defun autoinsert-packagename (fname)
  (let (ret (pdir (file-name-directory fname)))
    (while (and (not ret) (not (string= pdir "/")))
      (if (file-readable-p (concat pdir "CMakeLists.txt"))
          (setq ret (file-name-nondirectory (directory-file-name pdir))))
      (setq pdir (file-name-directory (directory-file-name pdir))))
    (if ret ret "")))
                
  


(setq d0-auto-insert-cc-header
  '((let ((pos (string-match "\\.\\([Hh]\\|hh\\|hpp\\)\\'" buffer-file-name)))
      (upcase (concat (autoinsert-symbol "guardprefix")
                      (file-name-nondirectory
                       (substring buffer-file-name 0 pos))
                      "_"
                      (substring buffer-file-name (1+ pos)))))
    "//" \n
    "// $Id"
    "$" \n
    "//" \n
    "// File: "
    (concat (autoinsert-symbol "fileprefix")
            (file-name-nondirectory buffer-file-name))
    \n
    "// Purpose: " \n
    "// Created: " 
    (format-time-string "%b, %Y, sss")
    \n
    "//" \n
    \n
    "#ifndef " str \n
    "#define " str "\n\n"
    (let ((ns (autoinsert-symbol "namespace")))
      (if ns
          (concat
           "\nnamespace " ns " {\n" "} // namespace " ns "\n")))
    _ "\n\n#endif // not " str \n ))


(setq atlas-auto-insert-cc-header
  '((let ((pos (string-match "\\.\\([Hh]\\|hh\\|hpp\\)\\'" buffer-file-name)))
      (upcase (concat ;(autoinsert-symbol "guardprefix")
                      (upcase (autoinsert-packagename buffer-file-name))
                      "_"
                      (file-name-nondirectory
                       (substring buffer-file-name 0 pos))
                      "_"
                      (substring buffer-file-name (1+ pos)))))

    "// This file's extension implies that it's C, but it's really -*- C++ -*-."
    \n
    "/*\n"
    " * Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration.\n"
    " */\n"
    "/**\n"
    " * @file "
    (concat ;(autoinsert-symbol "fileprefix")
            (autoinsert-packagename buffer-file-name) "/"
            (file-name-nondirectory buffer-file-name))
    "\n"
    " * @author scott snyder <snyder@bnl.gov>\n"
    " * @date " (format-time-string "%b, %Y") "\n"
    " * @brief \n"
    " */\n"
    \n
    \n
    "#ifndef " str \n
    "#define " str "\n\n"
    (let ((ns (autoinsert-symbol "namespace")))
      (if ns
          (concat
           "\nnamespace " ns " {\n" "} // namespace " ns "\n")))
    _ "\n\n#endif // not " str \n 
    '(c++-mode)
    ))


(setq d0-auto-insert-cc-body
  '(nil
    "//" \n
    "// $Id"
    "$" \n
    "//" \n
    "// File: "
    (concat (autoinsert-symbol "fileprefix")
            (file-name-nondirectory buffer-file-name))
    \n
    "// Purpose: " \n
    "// Created: " 
    (format-time-string "%b, %Y, sss")
    \n
    "//" \n
    \n
    "#include \""
    ;; nop without latest cc-mode
    (and (fboundp 'c-companion-file)
         ;;(file-readable-p (c-companion-file 'name))
         (file-name-nondirectory (c-companion-file 'name))) & ?\"
         | -10
         (let ((ns (autoinsert-symbol "namespace")))
           (if ns
               (concat
                "\nnamespace " ns " {\n" "} // namespace " ns "\n")))
         )
)



(setq atlas-auto-insert-cc-body
  '(nil
    "/*\n"
    " * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.\n"
    " */\n"
    "/**\n"
    " * @file "
    (concat (autoinsert-symbol "fileprefix")
            (file-name-nondirectory buffer-file-name))
    "\n"
    " * @author scott snyder <snyder@bnl.gov>\n"
    " * @date " (format-time-string "%b, %Y") "\n"
    " * @brief \n"
    " */\n"
    \n
    "#include \""
    ;; nop without latest cc-mode
    (and (fboundp 'c-companion-file)
         ;;(file-readable-p (c-companion-file 'name))
         (file-name-nondirectory (c-companion-file 'name))) & ?\"
         | -10
         (let ((ns (autoinsert-symbol "namespace")))
           (if ns
               (concat
                "\nnamespace " ns " {\n" "} // namespace " ns "\n")))
         )
)



(add-hook 'find-file-hooks 'auto-insert)
(setq auto-insert t)
(setq auto-insert-alist
      `((("\\.\\([Hh]\\|hh\\|hpp\\)\\'" . "C / C++ header")
         ;;,@d0-auto-insert-cc-header
         ,@atlas-auto-insert-cc-header
         )

        (("\\.\\([Cc]\\|cc\\|cpp\\|icc\\)\\'" . "C / C++ program")
         ;;,@d0-auto-insert-cc-body
         ,@atlas-auto-insert-cc-body
         )
        ))

