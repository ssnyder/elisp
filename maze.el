(defun abs (x)
  (if (< x 0)
      (- x)
    x))

(defun repeat-string (string reps)
  (let ((ans ""))
    (dotimes (i reps)
     (setq ans (concat ans string)))
    ans))

(defun rndi (i)
  (% (/ (abs (random)) 15000) i))

(defun randelt (lyst)
  (nth (rndi (length lyst)) lyst))

(defvar maze-wall-char ?#)
(defvar maze-undone-char ?*)
(defvar maze-void-char ? )

(defun maze-template (height width)
  (let ((solidline (make-string (1+ (* 2 width)) maze-wall-char))
	(perfline (concat (repeat-string
			   (concat (char-to-string maze-wall-char)
				   (char-to-string maze-undone-char))
			   width)
			  (char-to-string maze-wall-char))))
;  (let ((solidline (concat (repeat-string "+-" width) "+"))
;	(perfline (concat (repeat-string "|*" width) "|")))
    (dotimes (i height)
      (insert solidline ?\n perfline ?\n))
    (insert solidline)))

(defvar maze-up-disp 0)
(defvar maze-down-disp 0)
(defvar maze-right-disp 2)
(defvar maze-left-disp -2)
(defvar maze-linesize 0)
(defvar maze-minpos 0)
(defvar maze-maxpos 0)

(defun maze-make-offsets (height width)
  (setq maze-linesize (+ 2 (* 2 width)))
  (setq maze-up-disp (- (* 2 maze-linesize)))
  (setq maze-down-disp (* 2 maze-linesize))
  (setq maze-minpos (point-min))
  (setq maze-maxpos (* maze-linesize (1+ (* 2 height)))))

(defun maze-coords-to-pos (x y)
  (+ maze-minpos (1+ (* 2 x)) (* maze-linesize (1+ (* 2 y)))))

(defun maze-add-frontier (frontier elt)
  (if (or (< elt maze-minpos)
	  (> elt maze-maxpos)
	  (not (eq (char-after elt) maze-undone-char))
	  (memq elt frontier))
      frontier
    (cons elt frontier)))

(defun maze-void (pos)
  (goto-char pos)
  (delete-char 1)
  (insert-char maze-void-char 1))

(defun maze-add-neighbors (pos frontier)
  (setq frontier (maze-add-frontier frontier (+ pos maze-up-disp)))
  (setq frontier (maze-add-frontier frontier (+ pos maze-down-disp)))
  (setq frontier (maze-add-frontier frontier (+ pos maze-right-disp)))
  (setq frontier (maze-add-frontier frontier (+ pos maze-left-disp))))

(defun maze-add-dirlist (pos disp dirlist)
  (if (eq (char-after (+ pos disp)) maze-void-char)
      (cons (+ pos (/ disp 2)) dirlist)
    dirlist))

(defun maze-dirlist (pos)
  (let ((dirlist nil))
    (setq dirlist (maze-add-dirlist pos maze-up-disp dirlist))
    (setq dirlist (maze-add-dirlist pos maze-down-disp dirlist))
    (setq dirlist (maze-add-dirlist pos maze-right-disp dirlist))
    (setq dirlist (maze-add-dirlist pos maze-left-disp dirlist))))

(defun maze-make (height width)
  (let ((frontier nil) elt)
    (random t)
    (maze-template height width)
    (maze-make-offsets height width)
    (let ((seedpos (maze-coords-to-pos (rndi width) (rndi height))))
      (maze-void seedpos)
      (setq frontier (maze-add-neighbors seedpos frontier)))
    (while (not (null frontier))
      (setq elt (randelt frontier))
      (setq frontier (delq elt frontier))
      (maze-void elt)
      (maze-void (randelt (maze-dirlist elt)))
      (setq frontier (maze-add-neighbors elt frontier))))
  (let ((door1 (1- (maze-coords-to-pos 0 (rndi height)))) door2)
    (setq door2 (1+ (maze-coords-to-pos (1- width) (rndi height))))
    (maze-void door1)
    (maze-void door2)
    (list door1 door2)))

(defvar maze-mode-hook nil
  "If non-nil, its value is called on entry to Maze mode.")

(defvar maze-mode-map nil
  "Local keymap to use in Maze mode.")

(defun maze-rebind-function-key (char func)
  (let ((keys (all-function-key-sequences char)))
    (while keys
      (define-key maze-mode-map (car keys) func)
      (setq keys (cdr keys)))))

(if maze-mode-map nil
  (setq maze-mode-map (make-sparse-keymap))

  ;; Key bindings for cursor motion. Arrow keys are just "function"
  ;; keys, see below.
  (define-key maze-mode-map "h" 'maze-move-left)		; H
  (define-key maze-mode-map "l" 'maze-move-right)		; L
  (define-key maze-mode-map "j" 'maze-move-down)		; J
  (define-key maze-mode-map "k" 'maze-move-up)		; K
  (define-key maze-mode-map "\C-n" 'maze-move-down)		; C-N
  (define-key maze-mode-map "\C-p" 'maze-move-up)		; C-P
  (define-key maze-mode-map "\C-f" 'maze-move-right)	; C-F
  (define-key maze-mode-map "\C-b" 'maze-move-left)		; C-B

  ;; Key bindings for "function" keys. If your terminal has such
  ;; keys, make sure they are declared through the function-keymap
  ;; keymap (see file keypad.el).
  ;; One problem with keypad.el is that the function-key-sequence
  ;; function is really slow, so slow that you may want to comment out
  ;; the following lines ...
  (if (featurep 'keypad)
      (progn
	(maze-rebind-function-key ?u 'maze-move-up)	        ; Up Arrow
	(maze-rebind-function-key ?d 'maze-move-down)       ; Down Arrow
	(maze-rebind-function-key ?l 'maze-move-left)       ; Left Arrow
	(maze-rebind-function-key ?r 'maze-move-right)      ; Right Arrow
;;	(if (setq keys (function-key-sequence ?e))		; Enter
;;	    (define-key gomoku-mode-map keys 'gomoku-human-plays))
;;	(if (setq keys (function-key-sequence ?I))		; Insert
;;	    (define-key gomoku-mode-map keys 'gomoku-human-plays))
	)))

(defun maze-mode nil
  "Major maze mode.

Commands:
\\{maze-mode-map}
Entry to this mode calls the value of maze-mode-hook
if that value is non-nil."
  (interactive)
  (setq major-mode 'maze-mode
	mode-name "Maze")
  (use-local-map maze-mode-map)
  (run-hooks 'maze-mode-hook))

(defvar maze-entry nil)
(defvar maze-exit nil)

(defun maze-maybe-move (pos)
  (cond
   ((eq (char-after pos) maze-wall-char)
    (error "that's a wall, dufus!"))
   ((eq (char-after pos) ?\n)
    (error "stay in the maze, will you?"))
   ((eq pos maze-exit)
    (message "you made it!"))
   ((eq pos maze-entry)
    (message "no, this is where you started from!")))
  (goto-char pos))

(defun maze-move-up (count)
  (interactive "p")
  (dotimes (i count)
    (maze-maybe-move (+ (point) (/ maze-up-disp 2)))))

(defun maze-move-down (count)
  (interactive "p")
  (dotimes (i count)
    (maze-maybe-move (+ (point) (/ maze-down-disp 2)))))

(defun maze-move-right (count)
  (interactive "p")
  (dotimes (i count)
    (maze-maybe-move (+ (point) (/ maze-right-disp 2)))))

(defun maze-move-left (count)
  (interactive "p")
  (dotimes (i count)
    (maze-maybe-move (+ (point) (/ maze-left-disp 2)))))

(defun maze (&optional width height)
  (interactive)
  (switch-to-buffer "*maze*")
  (setq buffer-read-only nil)
  (maze-mode)
  (make-local-variable 'maze-entry)
  (make-local-variable 'maze-exit)
  (erase-buffer)
  (setq truncate-lines t)
  (if (or (null width)
	  (< width 1))
      (setq width (/ (- (window-width) 2) 2)))
  (if (or (null height)
	  (< height 1))
      (setq height (/ (- (window-height) 2) 2)))
  (let ((doorlist (maze-make height width)))
    (setq maze-entry (car doorlist))
    (setq maze-exit (car (cdr doorlist))))
  (goto-char maze-entry)
  (setq buffer-read-only t))
