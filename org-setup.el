;;;
;;; org-mode setup.
;;;

;; set load path, if needed

(add-to-list 'auto-mode-alist '("\\.org_archive$" . org-mode))
(require 'org-install)

;(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
;(global-set-key "\C-cb" 'org-iswitchb)


(setq org-todo-keywords
      '((sequence "TODO(t!)" "STARTED(s!)" "|" "DONE(d!/!)")
        (sequence "WAITING(w@/!)" "SOMEDAY(S!)"
                  "OPEN(O@)" "|" "CANCELLED(c@/!)")
        ))

(require 'remember)
;(org-remember-insinuate)

(global-set-key (kbd "C-M-r") 'org-remember)
(setq org-remember-store-without-prompt t)

(setq org-remember-default-headline "Tasks")

(setq org-remember-templates
      '(("todo" ?t "* TODO %?\n  %u\n  %a" nil bottom nil)
        ;("note" ?n "* %?                                        :NOTE:\n  :CLOCK:\n  :END:\n  %U\n  %a" nil bottom nil)
        ;("phone" ?p "* PHONE %:name - %:company -                :PHONE:\n  Contact Info: %a\n  %u\n  %?" nil bottom nil)
        ;("appointment" ?a "* %?\n  %U" "~/git/org/todo.org" "Appointments" nil)
        ;("org-protocol" ?w "* TODO Review %c%!\n  %U" nil bottom nil)
        ))
