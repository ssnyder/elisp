;;
;; Customizations for C++ editing.
;;

(require 'cc-mode)

(setq auto-mode-alist (cons '("\\.icc$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.tcc$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.tpp$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.ipp$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.hpp$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.cpp$" . c++-mode) auto-mode-alist))


(defun d0-lineup-comment (langelem)
  (if (or (assq 'ansi-funcdecl-cont c-syntactic-context)
          (assq 'func-decl-cont c-syntactic-context)
	  (assq 'knr-argdecl-intro  c-syntactic-context))
      -1000
    (c-lineup-comment langelem)))

(c-add-style "d0"
             '("gnu" ; similar to gnu
               (c-offsets-alist . ((substatement-open . 0)
                                   (comment-intro . d0-lineup-comment)
                                   (innamespace . 0)
                                   (member-init-cont . 0)
                                   ))
               (c-label-minimum-indentation . 0)
               ))

(setq c-mode-common-hook '(lambda () (c-set-style "d0")))

(define-key c++-mode-map "\C-cf" 'atlas-find-related-file)
(define-key c++-mode-map "\C-cv" 'atlas-find-related-file-other-window)
(define-key c-mode-map "\C-cf" 'atlas-find-related-file)
(define-key c-mode-map "\C-cv" 'atlas-find-related-file-other-window)
