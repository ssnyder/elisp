(require 'cl)

;;; cl- names in emacs 25 but not 23.
;(cl-defstruct atlas-context package-name package-dir package-path root relroot)
(defstruct atlas-context package-name package-dir package-path root relroot)

; directory-files-recursively in emacs 26 bit not 23.
(unless (fboundp 'directory-name-p)
  (defun directory-name-p (n)
    (string= (substring n -1) "/"))
)

(unless (fboundp 'directory-files-recursively)
  (defun directory-files-recursively (dir regexp &optional include-directories)
    "Return list of all files under DIR that have file names matching REGEXP.
This function works recursively.  Files are returned in \"depth first\"
order, and files from each directory are sorted in alphabetical order.
Each file name appears in the returned list in its absolute form.
Optional argument INCLUDE-DIRECTORIES non-nil means also include in the
output directories whose names match REGEXP."
    (let ((result nil)
          (files nil)
          ;; When DIR is "/", remote file names like "/method:" could
          ;; also be offered.  We shall suppress them.
          (tramp-mode (and tramp-mode (file-remote-p (expand-file-name dir)))))
      (dolist (file (sort (file-name-all-completions "" dir)
                          'string<))
        (unless (member file '("./" "../"))
          (if (directory-name-p file)
              (let* ((leaf (substring file 0 (1- (length file))))
                     (full-file (expand-file-name leaf dir)))
                ;; Don't follow symlinks to other directories.
                (unless (file-symlink-p full-file)
                  (setq result
                        (nconc result (directory-files-recursively
                                       full-file regexp include-directories))))
                (when (and include-directories
                           (string-match regexp leaf))
                  (setq result (nconc result (list full-file)))))
            (when (string-match regexp file)
              (push (expand-file-name file dir) files)))))
      (nconc result (nreverse files))))
)




(setq atlas-this-context nil)
(make-variable-buffer-local 'atlas-this-context)
(setq atlas-directory-contexts (make-hash-table :test 'equal))
(setq atlas-relroots (make-hash-table :test 'equal))


(defun atlas-dir-root (dir)
  "Given a directory within a test area, find the root directory for the area.
    (The one containing .asetup.)"
  (cond ((file-readable-p (concat dir ".asetup"))
         dir)
        ((file-readable-p (concat dir "SConstruct"))
         dir)
        (t
         (let ((parent (file-name-directory (directory-file-name dir))))
           (if (string= parent dir)
               dir
             (atlas-dir-root parent))))
        ))


(defun atlas-get-relroot (root)
  "Get the release root corresponding to a local root."
  (when (file-readable-p (concat root ".asetup"))
    (if (gethash root atlas-relroots)
        (gethash root atlas-relroots)
      (setq relroot (shell-command-to-string (concat "cm-relroot " root)))
      (if relroot (puthash root relroot atlas-relroots))
      relroot)
    )
  )


(defun atlas-make-package-context (dir)
  (let ((root (atlas-dir-root dir)))
    (make-atlas-context
     :package-dir dir
     :package-name (file-name-nondirectory (directory-file-name dir))
     :package-path (file-relative-name dir root)
     :root root
     :relroot (atlas-get-relroot root))
  ))


(defun atlas-context-from-dir (dir)
  (let ((ctx (gethash dir atlas-directory-contexts))
        (parent (file-name-directory (directory-file-name dir)))
        )
    (unless ctx
      (cond ((file-readable-p (concat dir "CMakeLists.txt"))
             (setq ctx (atlas-make-package-context dir)))
            ((file-readable-p (concat dir ".asetup"))
             nil)
            ((file-readable-p (concat dir "SConstruct"))
             nil)
            ((string= dir parent)
             nil)
            (t
             (setq ctx (atlas-context-from-dir parent))))
      (if ctx (puthash dir ctx atlas-directory-contexts))
      )
    ctx))
            
        

(defun atlas-buffer-context (buf)
  "Return the ATLAS context for a given buffer."
  (when atlas-this-context
    atlas-this-context)

  (setq atlas-this-context
        (atlas-context-from-dir
         (file-name-directory (buffer-file-name (current-buffer)))))

  )


(defun atlas-tofirst (p l)
  (let ((mem (member p l))
        (ll l))
    (cond ((null mem)
           l)
          ((eq mem l)
           l)
          (t
           (while (and ll (not (eq (cdr ll) mem)))
             (setq ll (cdr ll)))
           (if ll
               (progn
                 (setcdr ll nil)
                 (nconc mem l))
             l))
          )))
             


(defun atlas-get-related-files (buf)
  (let* ((ctx (atlas-buffer-context buf))
         (fname-full (buffer-file-name buf))
         (fname (file-name-nondirectory fname-full))
         (rootname (file-name-sans-extension fname))
         (ll (atlas-tofirst fname-full
                            (directory-files-recursively (atlas-context-package-dir ctx)
                                                         (concat "^" rootname "\\.[^~]*$")))))
    (if (and ll (string= (car ll) fname-full))
        (cdr ll)
      ll)
  ))
                                         
  
(defun atlas-find-related-file ()
  (interactive)
  (let ((other-files (atlas-get-related-files (current-buffer))))
    (unless other-files
      (error "No related files found."))
    (find-file (car other-files))
    ))


(defun atlas-find-related-file-other-window ()
  (interactive)
  (let ((other-files (atlas-get-related-files (current-buffer))))
    (unless other-files
      (error "No related files found."))
    (find-file-other-window (car other-files))
    ))



(defun query-replace-symbol-make-repl (symbol)
  (cond
   ((and (> (length symbol) 1)
         (string= (substring symbol 0 2) "m_"))
    (substring symbol 2))
   ((eq (string-to-char symbol) ?_)
    ;;(concat "m" symbol)
    (substring symbol 1)
    )
   (t
    (concat "m_" symbol))))

(defun query-replace-symbol-repl (data n)
  (let ((syntax (syntax-ppss)))
    (if (nth 3 syntax)
        (car data)
      (cdr data))))

(defun query-replace-symbol (symbol)
    ""
                                        ;(interactive "sSymbol: ")
    (interactive (list
                  (read-string (format "Symbol (%s): "
                                       (thing-at-point 'symbol))
                               nil nil (thing-at-point 'symbol))))
    ;(interactive (list
    ;              (semantic-complete-read-tag-buffer-deep "Symbol: ")))
    (let ((case-fold-search nil)
          (pat (concat "\\_<" symbol "\\_>"))
          (repl (query-replace-symbol-make-repl symbol)))
      (setq repl (read-from-minibuffer "Replacement: " repl))
      (perform-replace
       pat
       (cons (symbol-function 'query-replace-symbol-repl) (cons symbol repl))
       t ; query-flag
       t ; regexp-flag
       nil ; delimited-flag
       )))


(defun atlas-copyright-update ()
  (when (atlas-buffer-context (current-buffer))
    (copyright-update)))

(add-hook 'before-save-hook 'atlas-copyright-update)
(setq copyright-year-ranges t)
