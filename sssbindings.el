;;;;
;;;; sssbindings.el
;;;;

;;;;
;;;; Some of these routines came from gosmacs.el.
;;;;

(defun rebind-and-record (map bindings)
  "Establish many new bindings in keymap MAP and record the bindings replaced.
BINDINGS is an alist whose elements are (KEY DEFINITION).
Value is a similar alist whose elements describe the same KEYs
but each with the old definition that was replaced,"
  (let (old)
    (while bindings
      (let* ((this (car bindings))
	     (key (car this))
	     (newdef (nth 1 this)))
        ;(if (not (atom newdef)) (setq newdef (eval newdef)))
	(setq old (cons (list key (lookup-key map key)) old))
	(define-key map key newdef))
      (setq bindings (cdr bindings)))
    (nreverse old)))


(defun set-gnu-bindings ()
  "Restore the global bindings that were changed by \\[set-my-bindings]."
  (interactive)
  (rebind-and-record global-map gnu-binding-alist))


(defun gosmacs-previous-window ()
  "Select the window above or to the left of the window now selected.
From the window at the upper left corner, select the one at the lower right."
  (interactive)
  (select-window (previous-window)))


(defun gosmacs-next-window ()
  "Select the window below or to the right of the window now selected.
From the window at the lower right corner, select the one at the upper left."
  (interactive)
  (select-window (next-window)))


(defun scroll-one-line-up (&optional arg)
  "Scroll the selected window up (forward in the text) one line (or N lines)."
  (interactive "p")
  (scroll-up (or arg 1)))


(defun scroll-one-line-down (&optional arg)
  "Scroll the selected window down (backward in the text) one line (or N)."
  (interactive "p")
  (scroll-down (or arg 1)))



(defun shell-other-window ()
  "Run an inferior shell, with I/O through buffer *shell*, and
put it in another window. If buffer exists but shell process is
not running, make new shell. Also see 'shell'."
  (interactive)
  (let ((same-window-buffer-names
	 (delete "*shell*"
		 (copy-sequence same-window-buffer-names))))
    (shell)))


;; because I have so many screens open all the time, and idle C-xC-c can
;; really screw things up for me.  So make sure we dont exit without
;; confirmation, and rebind C-xC-c appropriately

(defun my-exit-from-emacs ()
  (interactive)
  (if (yes-or-no-p "Do you want to exit ")
      (save-buffers-kill-emacs)))


(defvar gnu-binding-alist nil)

;;;
;;; this is the list of global key bindings that i change
;;;

(defun set-my-bindings ()
  "Rebind some keys globally to make me happy.
Use \\[set-gnu-bindings] to restore previous global bindings."
  (interactive)

  ;; For emacs 21, we want to override bindings done here...
  (require 'font-lock)

  (setq gnu-binding-alist
	(rebind-and-record global-map
	 '(("\C-x\C-e" compile)                    ; eval-last-sexp
	   ("\C-x\C-v" find-file-other-window)     ; find-alternate-file
	   ("\C-x\C-z" shrink-window)              ; suspend-frame
	   ("\C-x!" shell)                         ; [not bound]
	   ("\C-x4!" shell-other-window)           ; [not bound]
	   ("\C-xn" gosmacs-next-window)           ; narrowing keymap?
	   ("\C-xp" gosmacs-previous-window)       ; [not bound]
	   ("\C-xz" enlarge-window)                ; repeat
	   ("\C-z" scroll-one-line-up)             ; suspend frame
	   ;("\eq" query-replace)                   ; fill-paragraph
           ; query-replace available as M-%.
	   ("\er" replace-string)                  ; move-to-window-line
	   ("\ez" scroll-one-line-down)            ; zap-to-char
	   ("\C-x\C-y" yow)                        ; [not bound]
           ("\C-x\C-c" my-exit-from-emacs)         ; [not bound]
	   ("\010" backward-delete-char-untabify)
	   ("\e?" help-command)
           ("\C-xm"   mh-smail)                    ; compose-mail
           ("\C-x4m"  mh-smail-other-window)       ; compose-mail-other-window

           ([kp-f1]          save-buffer)
           ([kp-subtract]    compile)
           ([kp-divide]      call-last-kbd-macro)
           ([kp-left]        backward-word)
           ([kp-right]       forward-word)
           ([kp-enter]       [return])
           ([kp-prior]       gosmacs-previous-window)
           ([kp-next]        gosmacs-next-window)
           ([kp-home]        other-frame)
           ([kp-up]          previous-error)
           ([kp-down]        next-error)
           ([f10]            save-buffer)

           ;; These are emacs standard 23 bindings.
           ;; They were different in emacs 21 and xemacs.
           ("\eo"            facemenu-keymap)
           ;("\eo\eo"         font-lock-fontify-block)
           ("\egg"           goto-line)
           ("\egn"           next-error)
           ("\egp"           previous-error)
           ("\eg\eg"         goto-line)
           ("\eg\en"         next-error)
           ("\eg\ep"         previous-error)
           )))

  ;; Set b to scroll back in info-mode and view-mode.
  (if (not (boundp 'Info-mode-map))
      (require 'info))
  (define-key Info-mode-map "b" 'scroll-down)
  (define-key Info-mode-map [backspace] 'scroll-down)
  (define-key Info-mode-map [delete] 'scroll-down)

  (cond ((maybe-require 'view) ; used in emacs
         (define-key view-mode-map "b" 'View-scroll-half-page-backward)
         (define-key view-mode-map [backspace] 'View-scroll-half-page-backward)
         (define-key view-mode-map [delete] 'View-scroll-half-page-backward)) 

        ((maybe-require 'view-less) ; used in xemacs
         (define-key view-mode-map "b" 'view-scroll-some-lines-down)
         (define-key view-mode-map [backspace] 'view-scroll-some-lines-down)
         (define-key view-mode-map [delete] 'view-scroll-some-lines-down)) 
	)
         
  ;; Make the delete key delete forward.
  (if (boundp 'delete-key-deletes-forward)
      (setq delete-key-deletes-forward t)
    (if (boundp 'shared-lisp-mode-map)
        (define-key shared-lisp-mode-map [delete] 'delete-char))
    (global-set-key [delete] 'delete-char)
    (global-set-key [backspace] 'backward-delete-char-untabify)
    (setq c-delete-function 'delete-char))

  (define-key isearch-mode-map [backspace] 'isearch-delete-char)
  (define-key isearch-mode-map [delete] 'isearch-delete-char)

  ;;
  ;; C-z gets overridden in the window system-specific maps.
  ;; So need to define it there, if they exist.
  ;;
  (if (boundp 'global-tty-map)
      (progn
        (define-key global-tty-map "\C-z" 'scroll-one-line-up)
        (define-key global-tty-map "\e`"  'suspend-emacs))
    (define-key global-map "\e`"  'suspend-emacs))
  (if (boundp 'global-window-system-map)
      (progn
        (define-key global-window-system-map "\C-z" 'scroll-one-line-up)))
  )



;; serial terminal setup.
(setq term-setup-hook '(lambda nil

;;  (term-xoff)
  (let ((term (getenv "TERM")))
          ;; DEC-type terminals
    (cond ((and (equal (downcase (substring term 0 2)) "vt")
		(not (eq window-system 'x)))
	   (progn
	     (enable-arrow-keys)
    
	     (setup-terminal-keymap CSI-map
	       '(("2~" . ?\C-a)  ; insert key -> BOL
		 ("3~" . ?P)     ; remove key -> prev. page
		 ("4~" . ?D)     ; select key -> del char
		 ("5~" . ?\C-b)  ; prev key   -> EOL
		 ("6~" . ?N)))   ; next key   -> next page
	     ))
	  ;; Iris console under wsh
	  ((and (equal term "iris-ansi")
		(not (eq window-system 'x)))
	   (enable-arrow-keys)
	   (wsh-fix-delete))))))


