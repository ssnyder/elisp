;;-
;; Register nnd0 with known select methods.

(gnus-declare-backend "nnd0" 'post 'address)

;;; Code:

(require 'nnoo)
(require 'nnweb)

(nnoo-declare nnd0)

(defvoo nnd0-buffer nil)

;;; Interface functions

(nnoo-define-basics nnd0)


(deffoo nnd0-request-list (&optional server)
  (nnd0-possibly-change-server nil server)
  (save-excursion
    (set-buffer nnd0-buffer)
    (nnd0-query "archive")
    (let (groups g gg)
      (goto-char (point-min))
      (while (re-search-forward "<b>ARCHIVE\\.\\([^ <]+\\) *</b>"
                                (point-max) t)
        (setq groups (cons (list (nnd0-buffer-match-substring 1)) groups)))
      (setq groups (nreverse groups))
      (setq gg groups)
      (while (setq g (pop gg))
        (setcdr g (nnd0-get-last (car g))))
      (setq foofoo groups) ;; XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      (set-buffer nntp-server-buffer)
      (erase-buffer)
      (while (setq g (pop groups))
        (insert (format "%s %d 1 n\n" (car g) (cdr g))))))
  t)

;;; ??? kill buffer on close

(nnoo-define-skeleton nnd0)


;;; Internal functions

(defun nnd0-url (query)
  (let ((url (concat "http://" (nnoo-current-server 'nnd0) "/cgi-bin/d0news")))
    (if (not (zerop (length query)))
        (setq url (concat url "?" query)))
    url))
  

(defun nnd0-possibly-change-server (&optional group server)
  (nnd0-init server)
  (when server
    (unless (nnd0-server-opened server)
      (nnd0-open-server server))))


(defun nnd0-init (server)
  "Initialize buffers and such."
  (unless (gnus-buffer-live-p nnd0-buffer)
    (setq nnd0-buffer
	  (save-excursion
	    (nnheader-set-temp-buffer
	     (format " *nnd0 %s*" server))))))



(defun nnd0-query (query)
  ;; ??? Borrows nnweb's buffer.
  ;; ??? Doesn't work in async mode.
  (nnweb-fetch-url (nnd0-url query)))


(defun nnd0-buffer-match-substring (&optional i) "\
Returns the text in the current buffer matched by subexp I of last search"
  (if (null i) (setq i 0))
  (buffer-substring
   (match-beginning i)
   (match-end i)))


(defun nnd0-get-last (group)
  (save-excursion
    (set-buffer nnd0-buffer)
    (nnd0-query (format "list__%s" group))
    (let ((msgs (nnd0-parse-headers group)))
      (when (not msgs)
        (nnd0-query (format "list__%s__a" group))
        (setq msgs (nnd0-parse-headers group)))
        
      (if msgs
          (caar (nreverse msgs))
        999))))


(defun nnd0-parse-headers (group)
  (let (msgs
        (re (format "\\?read__%s__\\([0-9]+\\)\">\\([^<]+\\)</A> \\(.*?\\) *-- <b>\\([^<]*\\)</b>" group)))
    (goto-char (point-min))
    (while (re-search-forward re (point-max) t)
      (setq msgs (cons (list (nnd0-clean-number
                              (nnd0-buffer-match-substring 1))
                             (nnd0-buffer-match-substring 2)
                             (nnd0-buffer-match-substring 4)
                             (nnd0-buffer-match-substring 3)) msgs)))
    msgs))


(defun nnd0-clean-number (num)
  (string-to-number num))
