;;; muse-autoloads.el --- autoloads for Muse
;;
;;; Code:

;;;### (autoloads (muse-latex-transform) "muse-convert" "muse-convert.el"
;;;;;;  (19324 26550))
;;; Generated autoloads from muse-convert.el

(autoload 'muse-latex-transform "muse-convert" "\
Not documented

\(fn)" t nil)

;;;***

;;;### (autoloads (muse-message-markup) "muse-message" "../experimental/muse-message.el"
;;;;;;  (19324 26550))
;;; Generated autoloads from ../experimental/muse-message.el

(autoload 'muse-message-markup "muse-message" "\
Markup a wiki-ish e-mail message as HTML alternative e-mail.
This step is manual by default, to give the author a chance to review
the results and ensure they are appropriate.
If you wish it to be automatic (a risky proposition), just add this
function to `message-send-hook'.

\(fn)" t nil)

;;;***

;;;### (autoloads (muse-insert-tag muse-index muse-what-changed muse-previous-reference
;;;;;;  muse-next-reference muse-follow-name-at-point-other-window
;;;;;;  muse-follow-name-at-point muse-browse-result muse-edit-link-at-point
;;;;;;  muse-mode) "muse-mode" "muse-mode.el" (19324 26550))
;;; Generated autoloads from muse-mode.el

(autoload 'muse-mode "muse-mode" "\
Muse is an Emacs mode for authoring and publishing documents.
\\{muse-mode-map}

\(fn)" t nil)

(autoload 'muse-edit-link-at-point "muse-mode" "\
Edit the current link.
Do not rename the page originally referred to.

\(fn)" t nil)

(autoload 'muse-browse-result "muse-mode" "\
Visit the current page's published result.

\(fn STYLE &optional OTHER-WINDOW)" t nil)

(autoload 'muse-follow-name-at-point "muse-mode" "\
Visit the link at point, or insert a newline if none is found.

\(fn &optional OTHER-WINDOW)" t nil)

(autoload 'muse-follow-name-at-point-other-window "muse-mode" "\
Visit the link at point in other window.

\(fn)" t nil)

(autoload 'muse-next-reference "muse-mode" "\
Move forward to next Muse link or URL, cycling if necessary.

\(fn)" t nil)

(autoload 'muse-previous-reference "muse-mode" "\
Move backward to the next Muse link or URL, cycling if necessary.
This function is not entirely accurate, but it's close enough.

\(fn)" t nil)

(autoload 'muse-what-changed "muse-mode" "\
Show the unsaved changes that have been made to the current file.

\(fn)" t nil)

(autoload 'muse-index "muse-mode" "\
Display an index of all known Muse pages.

\(fn)" t nil)

(autoload 'muse-insert-tag "muse-mode" "\
Insert a tag interactively with a blank line after it.

\(fn TAG)" t nil)

;;;***

;;;### (autoloads (muse-project-publish muse-project-find-file) "muse-project"
;;;;;;  "muse-project.el" (19324 26550))
;;; Generated autoloads from muse-project.el

(autoload 'muse-project-find-file "muse-project" "\
Open the Muse page given by NAME in PROJECT.
If COMMAND is non-nil, it is the function used to visit the file.
If DIRECTORY is non-nil, it is the directory in which the page
will be created if it does not already exist.  Otherwise, the
first directory within the project's fileset is used.

\(fn NAME PROJECT &optional COMMAND DIRECTORY)" t nil)

(autoload 'muse-project-publish "muse-project" "\
Publish the pages of PROJECT that need publishing.

\(fn PROJECT &optional FORCE)" t nil)

;;;***

;;;### (autoloads (muse-browse-url) "muse-protocols" "muse-protocols.el"
;;;;;;  (19324 26550))
;;; Generated autoloads from muse-protocols.el

(autoload 'muse-browse-url "muse-protocols" "\
Handle URL with the function specified in `muse-url-protocols'.
If OTHER-WINDOW is non-nil, open in a different window.

\(fn URL &optional OTHER-WINDOW)" t nil)

;;;***

;;;### (autoloads (muse-publish-this-file muse-publish-file) "muse-publish"
;;;;;;  "muse-publish.el" (19324 26550))
;;; Generated autoloads from muse-publish.el

(autoload 'muse-publish-file "muse-publish" "\
Publish the given FILE in a particular STYLE to OUTPUT-DIR.
If the argument FORCE is nil, each file is only published if it is
newer than the published version.  If the argument FORCE is non-nil,
the file is published no matter what.

\(fn FILE STYLE &optional OUTPUT-DIR FORCE)" t nil)

(autoload 'muse-publish-this-file "muse-publish" "\
Publish the page in the current file.

\(fn STYLE OUTPUT-DIR &optional FORCE)" t nil)

;;;***

;;;### (autoloads (muse-registry-initialize) "muse-registry" "muse-registry.el"
;;;;;;  (19324 26550))
;;; Generated autoloads from muse-registry.el

(autoload 'muse-registry-initialize "muse-registry" "\
Set `muse-registry-alist' from `muse-registry-file'.
If `muse-registry-file' doesn't exist, create it.
If FROM-SCRATCH is non-nil, make the registry from scratch.

\(fn &optional FROM-SCRATCH)" t nil)

;;;***

;;;### (autoloads nil nil ("../contrib/cgi.el" "../contrib/httpd.el"
;;;;;;  "../experimental/muse-cite.el" "../experimental/muse-mathml.el"
;;;;;;  "muse-book.el" "muse-colors.el" "muse-docbook.el" "muse-groff.el"
;;;;;;  "muse-html.el" "muse-http.el" "muse-journal.el" "muse-latex.el"
;;;;;;  "muse-latex2png.el" "muse-poem.el" "muse-regexps.el" "muse-texinfo.el"
;;;;;;  "muse-wiki.el" "muse-xml-common.el" "muse-xml.el" "muse.el")
;;;;;;  (19324 26567 960767))

;;;***

;;;### (autoloads (muse-blosxom-new-entry) "muse-blosxom" "muse-blosxom.el"
;;;;;;  (19324 26550))
;;; Generated autoloads from muse-blosxom.el

(autoload 'muse-blosxom-new-entry "muse-blosxom" "\
Start a new blog entry with given CATEGORY.
The filename of the blog entry is derived from TITLE.
The page will be initialized with the current date and TITLE.

\(fn CATEGORY TITLE)" t nil)

;;;***

(provide 'muse-autoloads)
;;; muse-autoloads.el ends here
;;
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:

