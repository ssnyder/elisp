;;;
;;; common part of .emacs.
;;;

(load "functions")
 (load "sssbindings")
 (set-my-bindings)
 (load "names")
 (load "atlas")
 (load "emacs-options")
 (load "emacs-patches")
 (load "font-options")
 (load "local-c-style")
 (load "my-auto-insert")
 (load "ud0")
 (load "epasetup")

 (if (maybe-require 'org-install)
     (load "org-setup"))
 (global-set-key "\C-cl" 'org-store-link)
 (global-set-key "\C-cc" 'org-capture)
 (global-set-key "\C-ca" 'org-agenda)
 (global-set-key "\C-cb" 'org-iswitchb)
 (setq org-catch-invisible-edits 'show)

 (autoload 'flame "flame"
  "Generate ARG (default 1) sentences of half-crazed gibberish."
  t nil)

 (unless (maybe-require 'remember)
   (setq load-path (cons (substitute-in-file-name "$HOME/elisp/remember") load-path))
   (require 'remember))

 ;(unless (maybe-require 'muse-mode)
 ;  (setq load-path (cons (substitute-in-file-name "$HOME/elisp/muse/lisp") load-path))
 ;  (setq load-path (cons (substitute-in-file-name "$HOME/elisp/muse/contrib") load-path))
 ;  (require 'muse-mode))


(setq load-path (cons (substitute-in-file-name "$HOME/elisp/rust-mode") load-path))
(autoload 'rust-mode "rust-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))


 (display-time)


 ;;;;;;;;;;;;;;;;;;;;;;;;;
 (setq load-home-init-file t) ; don't load init file from ~/.xemacs/init.el
