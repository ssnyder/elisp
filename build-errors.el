;; fixme --- build up of directory stack, since we don't have a leave

;(defvar build-errors-mode-map
;  (let ((map (make-sparse-keymap)))
;    (define-key map "n" 'build-errors-next-region)
;    map)
;  "Keymap for `build-errors-mode'.")


(defvar build-errors-marker-regexp
  "^\\*\\*\\* \\([a-zA-Z0-9_/]+\\)$"
  "Marker to separate compilation for different packages.")


(defun build-errors-filename-function-1 (path)
  (let ((dir (previous-single-property-change (point) 'directory)))
    (if dir
        (setq dir (car (get-text-property (1- dir) 'directory))))
    (if (and (not (file-name-absolute-p path)) dir)
        (expand-file-name 
         (concat 
          (file-name-as-directory
           (concat (file-name-as-directory dir) "cmt"))
          path))
      path)))

(defun build-errors-filename-function (path)
  (let ((ret (build-errors-filename-function-1 path))
        (dir (previous-single-property-change (point) 'directory)))
    (if dir
        (setq dir (car (get-text-property (1- dir) 'directory))))
    ret))


(define-derived-mode build-errors-mode nil "Build Errors"
  "Mode for handling build errors.

\\{build-errors-mode-map}"

  (set (make-local-variable 'build-errors-regions)
                            (build-errors-find-regions))
  (set (make-local-variable 'build-errors-region) nil)
  (set (make-local-variable 'build-errors-dir) nil)
  (set (make-local-variable 'compilation-directory-matcher)
       (list build-errors-marker-regexp '(1 . nil)))
  (set (make-local-variable 'compilation-parse-errors-filename-function)
       'build-errors-filename-function)
  (compilation-minor-mode t)

  (define-key build-errors-mode-map "n" 'build-errors-next-region)
  (define-key build-errors-mode-map "N" 'build-errors-next-nonempty-region)
  (define-key build-errors-mode-map "p" 'build-errors-previous-region)
  (define-key build-errors-mode-map "P" 'build-errors-previous-nonempty-region)
  (define-key build-errors-mode-map "q" 'build-errors-reset-region)

  (set (make-local-variable 'mode-line-position)
       (append mode-line-position
               '((:eval (build-errors-modeline)))))
  )


(defun build-errors-modeline nil
  (let ((ireg (if (null build-errors-region)
                  0
                (1+ build-errors-region))))
    (format " %d/%d" ireg (length build-errors-regions))))


(defun build-errors-reset-region ()
  (setq build-errors-region nil)
  (setq build-errors-dir nil)
  (widen))


(defun build-errors-view-region (reg)
  (goto-char (car reg))
  (setq build-errors-dir 
        (if (looking-at build-errors-marker-regexp)
            (buffer-substring-no-properties (match-beginning 1) (match-end 1))))
  (narrow-to-region (car reg) (cdr reg))
  (setq compilation-current-error nil)
  ;(compilation-minor-mode t)
  )


(defun build-errors-view-current-region ()
  (let ((reg (elt build-errors-regions build-errors-region)))
    (build-errors-view-region (car reg) (cdr reg))))


(defun build-errors-empty-region-p (reg)
  (save-excursion
    (goto-char (car reg))
    (forward-line)
    (>= (point) (cdr reg))))
  


(defun build-errors-bump-region-forward ()
  (setq build-errors-region
        (if (null build-errors-region)
            0
          (1+ build-errors-region)))
  (if (< build-errors-region (length build-errors-regions))
      (elt build-errors-regions build-errors-region)
    nil))


(defun build-errors-next-region ()
  (interactive)
  (widen)
  (let ((reg (build-errors-bump-region-forward)))
    (if reg
        (build-errors-view-region reg)
      (message "No more compilation markers.")
      (build-errors-reset-region))))


(defun build-errors-next-nonempty-region ()
  (interactive)
  (widen)
  (let (reg)
    (while (and (setq reg (build-errors-bump-region-forward))
                (build-errors-empty-region-p reg))
      t)
    (if reg
        (build-errors-view-region reg)
      (message "No more compilation markers.")
      (build-errors-reset-region))))


(defun build-errors-bump-region-backward ()
  (setq build-errors-region
        (if (null build-errors-region)
            (1- (length build-errors-regions))
          (1- build-errors-region)))
  (if (>= build-errors-region 0)
      (elt build-errors-regions build-errors-region)
    nil))


(defun build-errors-previous-region ()
  (interactive)
  (widen)
  (let ((reg (build-errors-bump-region-backward)))
    (if reg
        (build-errors-view-region reg)
      (message "No more compilation markers.")
      (build-errors-reset-region))))


(defun build-errors-previous-nonempty-region ()
  (interactive)
  (widen)
  (let (reg)
    (while (and (setq reg (build-errors-bump-region-backward))
                (build-errors-empty-region-p reg))
      t)
    (if reg
        (build-errors-view-region reg)
      (message "No more compilation markers.")
      (build-errors-reset-region))))

(defun build-errors-find-region-marks ()
  (let ((out nil))
    (save-excursion
      (save-restriction
        (widen)
        (goto-char (point-min))
        (while (re-search-forward build-errors-marker-regexp (point-max) t)
          (beginning-of-line)
          (setq out (cons (copy-marker (point)) out))
          (forward-line))))
    (cons (copy-marker (point-max)) out)))


(defun build-errors-find-regions ()
  (let ((out nil) (marks (build-errors-find-region-marks)))
    (while (cdr marks)
      (setq out (cons (cons (cadr marks) (car marks)) out))
      (setq marks (cdr marks)))
    (vconcat out)))
