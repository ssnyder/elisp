;;;;
;;;; d0.el
;;;;

;;;
;;; software lookup code
;;;

(defun D0-vff (fname)
  (if (eq system-type 'vax-vms)
      fname

    (let ((tmpbuf (generate-new-buffer " vff")) res)
      (save-excursion
        (set-buffer tmpbuf)
        (if (zerop (call-process "vff" nil tmpbuf nil fname))
            (setq res (buffer-substring (point-min) (1- (point-max))))))
      (kill-buffer tmpbuf)
      res)
    ))

(defvar D0-entry-file "d0$docs:entry_point.lis" "\
The name of the file containing the D0 entry point listing")

(defvar D0-software-buffer nil "\
The buffer containing the D0 entry point listing")

(defconst fortran-identifier-chars "a-zA-Z$_0-9" "\
The set of characters in permissable fortran identifiers.")

(defun bsearch (word) nil "\
Search for a line in the current buffer using a binary search.
Each line should start with a key, delimited by whitespace.
Returns t if the search was successful, nil otherwise.
Point is left after the last key tested."
  (let ((lo (point-min))
	(hi (progn
	      (goto-char (point-max))
	      (beginning-of-line)
	      (point)))
	mid bufstr)
    (setq word (upcase word))
    (catch 'done
      (while t
	(setq mid (progn
		    (goto-char (/ (+ lo hi) 2))
		    (beginning-of-line)
		    (point)))
	(setq bufstr
	      (upcase
	       (buffer-substring
		(point)
		(progn
		  (skip-chars-forward fortran-identifier-chars)
		  (point)))))
	(if (string= word bufstr)
	    (throw 'done t)
	  (progn
	    (if (= mid lo)
		(let ((case-fold-search t))
		  (goto-char hi)
		  (end-of-line)
		  (setq hi (point))
		  (goto-char lo)
		  (forward-line)
		  (throw 'done
			 (re-search-forward
			  (concat "^" (regexp-quote word) "\\b[^_]")
			  hi t)))
	      (if (string< word bufstr)
		  (setq hi mid)
		(setq lo mid)))))
	))))

(defun D0-lookup-name (name)
  "*Returns the name of the file containing entry point NAME"
  (interactive "sName: ")
  (save-excursion
    (set-buffer (if (and D0-software-buffer
			 (buffer-name D0-software-buffer))
		    D0-software-buffer
		  (setq D0-software-buffer
			(find-file-noselect (D0-vff D0-entry-file)))))
    (goto-char (point-min))
    (if (not (stringp name))
	(error "NAME must be a string!"))
    (setq name (substring name 0 (min 20 (length name))))
    (if (bsearch name)
	(let (beg str)
	  (D0-check-uniqueness name)
	  (skip-chars-forward " \011")
	  (setq beg (point))
	  (end-of-line)
	  (setq str (buffer-substring beg (point)))
	  (if (interactive-p)
	      (message
	       (format "`%s' is in file `%s'" name str)))
	  str)
      (error "`%s' not found in the D0 software" name 1)
      )))

(defun D0-check-uniqueness (name) "\
Make sure that there's only one entry for NAME. If there's more than one,
pop up a window to let the user choose."
  (let ((case-fold-search t)
	(oldpoint (point))
	(unique t)
	(name-regexp (concat "^" (regexp-quote name) "[ \t]"))
	beg)
    (forward-line -1)
    (while (and (not (bobp))
		(looking-at name-regexp))
      (forward-line -1)
      (setq unique nil))
    (forward-line)
    (setq beg (point))
    (goto-char oldpoint)
    (forward-line)
    (while (looking-at name-regexp)
      (forward-line)
      (setq unique nil))
    (if unique
	(goto-char oldpoint)
      (D0-choose-entry beg (point)))))

(defun D0-lines-identical-p nil "\
Returns t if the current line is identical to the next one."
  (save-excursion
    (beginning-of-line)
    (let ((beg (point))
	  str1)
      (end-of-line)
      (setq str1 (buffer-substring beg (point)))
      (if (zerop (forward-line))
	  (string= str1
		   (buffer-substring (point)
				     (progn
				       (end-of-line)
				       (point))))))))

(defun D0-trim-duplicate-lines nil "\
Trim duplicate lines off of the beginning and end of the buffer by narrowing."
  (goto-char (point-min))
  (while (D0-lines-identical-p)
    (forward-line))
  (narrow-to-region (point) (point-max))
  (goto-char (point-max))
  (forward-line -2)
  (while (D0-lines-identical-p)
    (forward-line -1))
  (forward-line 2)
  (narrow-to-region (point-min) (point)))

(defun D0-choose-entry (beg end) "\
Pop up a window to let the user choose one of the lines in the current buffer
between BEG and END. The current buffer is assumed to be D0-software-buffer;
point is left after the chosen word."
  (save-window-excursion
    (save-restriction
      (let ((old-local-map (current-local-map))
	    (choose-map (make-sparse-keymap)))
	(define-key choose-map "\n" 'D0-choose-key)
	(define-key choose-map "\r" 'D0-choose-key)
	(define-key choose-map " " 'D0-choose-key)
	(narrow-to-region beg end)
	(D0-trim-duplicate-lines)
	(if (= 1 (count-lines (point-min) (point-max)))
	    nil
	  (pop-to-buffer D0-software-buffer)
	  (use-local-map choose-map)
	  (goto-char (point-min))
	  (message
        "Multiple entries present. Move cursor to desired line and press RET.")
	  (recursive-edit)
	  (use-local-map (current-local-map)))
	(beginning-of-line)
	(if (eobp) (forward-line -1))
	(skip-chars-forward fortran-identifier-chars)))))

(defun D0-choose-key nil "\
*Choose one D0 software library entry."
  (interactive)
  (exit-recursive-edit))

(defun read-no-blanks-default (prompt default) ""
  (let ((inp (read-string
	      (concat prompt " [" default "]: ")
	      "")))
    (if (string= "" inp)
	default
      inp)))

(defun D0-get-name nil "\
Prompt for a routine name; use the closest name in the buffer as a default."
  (save-excursion
    (skip-chars-backward fortran-identifier-chars)
    (skip-chars-forward " \t")
    (let ((default (upcase (buffer-substring (point)
					     (progn
					       (forward-word 1)
					       (point))))))
      (if (string= default "CALL")
	  (progn
	    (forward-char)
	    (skip-chars-forward " \t")
	    (D0-get-name))
	(read-no-blanks-default "Routine to find" default)))))

(defun D0-find-function nil "\
*Prompts for a D0 function name and loads the file containing it."
  (interactive)
  (let ((name (D0-get-name))
	filename)
    (setq filename (D0-vff (D0-lookup-name name)))
    (if (not filename)
        (error "Can't find file %s" filename)
      (find-file-other-window filename)
      (goto-char (point-min))
      (re-search-forward (fortran-routine-regexp name) nil t)
      (beginning-of-line)
      (message "Found `%s' in file %s." name filename))))

;;;
;;; code to produce D0 headers
;;;

(defvar D0-author-name "Joe Schmoe" "\
*Name to insert in the ``Created'' line in the D0 procedure header")

(defun D0-header-ins-sepline nil
  (insert
   "C----------------------------------------------------------------------\n")
  )

(defun D0-header-line (text)
  (insert "C-")
  (if (not (string= "" text))
      (insert (concat "   " text)))
  (insert "\n"))

(defun D0-header-blank-line nil
  (D0-header-line ""))

(defun D0-date nil "\
Return the current date in the format DD-MMM-YYYY."
  (let ((timestr (current-time-string)))
    ;; I assume that current-time-string always returns a string
    ;; in the format: Sat May 26 17:58:35 1990
    (concat
     (substring timestr 8 10)
     "-"
     (upcase (substring timestr 4 7))
     "-"
     (substring timestr 20 24))))

(defun D0-header-created-line nil
  (D0-header-line
   (concat
    "Created  "
    (D0-date)
    "   "
    D0-author-name)))

;; Create a D0 comment header and frame

(defun D0-header (type) "\
*Create a D0 header frame.\n
TYPE should be one of 'program, 'subroutine, 'function, or 'common.
The header is inserted after point, except for 'common headers, which
are placed before point."
  (interactive "SType of header (program, subroutine, function, common): ")
  (save-excursion
    (if (equal type 'common)
	(progn
	  (beginning-of-line)
	  (D0-header-ins-sepline)
	  (D0-header-blank-line)
	  (D0-header-created-line)
	  (D0-header-blank-line)
	  (D0-header-ins-sepline))
      (progn
	(end-of-line)
	(insert "\n")
	(D0-header-ins-sepline)
	(D0-header-blank-line)
	(D0-header-line "Purpose and Methods : ")
	(D0-header-blank-line)
	(if (equal type 'function)
	    (D0-header-line "Returned value  : "))
	(D0-header-line "Inputs  : ")
	(D0-header-line "Outputs : ")
	(D0-header-line "Controls: ")
	(D0-header-blank-line)
	(D0-header-created-line)
	(D0-header-blank-line)
	(D0-header-ins-sepline)
	(insert "      IMPLICIT NONE\n")
	(D0-header-ins-sepline)
	(if (not (equal type 'program))
	    (insert "  999 RETURN\n"))
	(insert "      END\n"))
      )))

(defun D0-program-header nil "\
*Insert a standard D0 program header"
  (D0-header 'program)
  (fortran-indent-line))

(defun D0-subroutine-header nil "\
*Insert a standard D0 subroutine header"
  (D0-header 'subroutine)
  (fortran-indent-line))

(defun D0-function-header nil "\
*Insert a standard D0 function header"
  (D0-header 'function)
  (fortran-indent-line))

(defun D0-common-header nil "\
*Insert a standard D0 common block header"
  (D0-header 'common)
  (fortran-indent-line))

;;;
;;; customize fortran-mode
;;;

;; some of these redefine procedures in fortran.el, so load that first...

;; (if (or (not (fboundp 'fortran-mode))
;; 	(and (listp (symbol-function 'fortran-mode))
;; 	     (eq (car (symbol-function 'fortran-mode)) 'autoload)))
;;     (load-library "fortran"))

(defconst fortran-end-regexp "^[ \t0-9]*end\\b[ \t]*[^ \t=(a-z]" "\
Regexp used to search for a fortran END statement")

(defconst fortran-modstart-regexp
  "^[^cC!][ \t]*\
\\(subroutine\\|program\\|[^!\n]*function\\|entry\\|block *data\\)\\b\
[ \t]+\\([a-z$_]+[a-z$_0-9]*\\)\\b" "\
Regexp used to search for a fortran module header statement.
Matches SUBROUTINE, PROGRAM, or FUNCTION, BLOCK DATA, and ENTRY statements.\
")

(defconst fortran-regexp-paren-regexp "\\\\(.*\\\\)" "\
Regexp used to find a parenthesized expression in another regexp")

(defun fortran-routine-regexp (routine) "\
Return a regexp that will match the header line for routine ROUTINE"
  (string-match fortran-regexp-paren-regexp fortran-modstart-regexp
	(string-match fortran-regexp-paren-regexp fortran-modstart-regexp))
  (concat
   (substring fortran-modstart-regexp 0 (match-beginning 0))
   (regexp-quote routine)
   (substring fortran-modstart-regexp (match-end 0))))

(defconst fortran-include-regexp
  "^[ \t]*include\\b[ \t]+'\\([^'\n]+\\)'" "\
Regexp used to search for fortran INCLUDE statements.")

(defconst fortran-special-regexp
  (concat "^\C-l$\\|" fortran-include-regexp) "\
Regexp used to search for fortran INCLUDE statements or form feeds")

(defun buffer-match-substring (&optional i) "\
Returns the text in the current buffer matched by subexp I of last search"
  (if (null i) (setq i 0))
  (buffer-substring
   (match-beginning i)
   (match-end i)))

(defun fortran-find-routine-name1 nil
  (let ((case-fold-search t))
    (save-excursion
      (if (re-search-forward fortran-modstart-regexp nil t)
	  (buffer-match-substring 2)
	"(null)"))))

;; this should be able to extract files from TLB's but it can't.
(defun fortran-find-include-file (name) "\
Loads the include file NAME into a buffer, makes the buffer current (but
not selected), and returns it.
Signals an error if the file does not exist."
;
; VMS fortrash allows switches following the file name (such as /list).
;  trash those suckers.
;
  (if (eq system-type 'vax-vms)
      (let ((ndx (string-match "/" name)))
	(if ndx (setq name (substring name 0 ndx)))))
  (if (and (eq system-type 'vax-vms)
	   (string-match "[ \t]*\\([^ \t]*\\)[ \t]*(\\(.*\\))[ \t]*" name))
      (let ((old-buffer (current-buffer))
	    (libname (substring name (match-beginning 1) (match-end 1))))
	(setq name (substring name (match-beginning 2) (match-end 2)))
	(if (zerop (length libname))
	    (setq libname "sys$library:forsysdef.tlb"))
	(set-buffer (get-buffer-create (concat name ".inc")))
	(if (zerop (buffer-size))
	    (progn
;	      (subprocess-command-to-buffer
;	       (concat "library/text/output=sys$output/extract="
;		       name
;		       " "
;		       libname)
;	       (current-buffer))
	      (call-process "library" nil t nil
			    "/text/output=sys$output"
			    (concat "/extract=" name)
			    libname)
	      (if (zerop (buffer-size))
		  (progn
		    (kill-buffer (current-buffer))
		    (set-buffer old-buffer)
		    (error "Module `%s' doesn't seem to be in FORSYSDEF."
			   name)))
	      ; get rid of spurious blank line at end...
	      (goto-char (point-max))
	      (forward-line -1)
	      (delete-region (point) (point-max))
	      (fortran-mode)
	      (set-buffer-modified-p nil)))
	(current-buffer))
    (setq name (D0-vff name))
    (if (not (file-readable-p name))
	(error "I don't seem to be able to read INCLUDE file %s" name))
    (set-buffer (find-file-noselect name))))

(defun fortran-follow-include1 nil "\
Loads the file specified in an include directive on the current line into
a buffer and returns it."
   (fortran-find-include-file
    (save-excursion
      (beginning-of-line)
      (if (looking-at fortran-include-regexp)
	  (buffer-match-substring 1)
	(error
	 "There doesn't seem to be any INCLUDE directive on this line...")
	))))

(defun fortran-follow-include nil "\
*Follows an INCLUDE reference on the current line."
  (interactive)
  (switch-to-buffer
   (fortran-follow-include1)))

(defun fortran-follow-include-other-window nil "\
*Follows an INCLUDE reference on the current line in another window."
  (interactive)
  (switch-to-buffer-other-window
   (fortran-follow-include1)))

(defun fortran-find-routine-name nil "\
Return the fortran routine name in which point is currently located"
  (let ((case-fold-search t))
    (save-excursion
      (beginning-of-line)
      (re-search-backward fortran-end-regexp nil 'move)
      (if (looking-at fortran-end-regexp)
	  (forward-line))
      (fortran-find-routine-name1))))

(defun fortran-count-include-lines (name) "\
Count the number of lines in include file NAME"
  (save-excursion
    ;; .zebinc files don't get expanded.
    (if (string-match "\\.zebinc$" name)
	0
      (fortran-find-include-file name)
      (fortran-count-lines (point-min) (point-max)))))

(defun fortran-count-lines (startpos endpos) "\
Count the number of fortran lines in the given region"
  (let ((case-fold-search t)
	nlines)
    (save-excursion
      (setq nlines (count-lines startpos endpos))
      (goto-char startpos)
      (while (re-search-forward fortran-special-regexp endpos t)
	(if (= (char-after (match-beginning 0)) ?\C-l)
	    (setq nlines (1- nlines))
	  (setq nlines (+ nlines
			  (fortran-count-include-lines
			   (buffer-match-substring 1))))))
      nlines)))

(defun fortran-what-line nil "\
*Return the fortran line and routine on which point is currently situated"
  (interactive)
  (let ((case-fold-search t)
	(oldpoint (point))
	routine-name nlines)
    (save-excursion
      (beginning-of-line)
      (re-search-backward fortran-end-regexp nil 'move)
      (if (looking-at fortran-end-regexp)
	  (forward-line))
      (setq routine-name (fortran-find-routine-name1))
      (setq nlines (1+ (fortran-count-lines (point) oldpoint)))
      (if (interactive-p)
	  (message
	   (concat "Line " nlines " in routine " routine-name))
	(list nlines routine-name)))
    ))

(defun fortran-forward-line1 (&optional i) "\
Like forward-line, except it gives an error if there aren't enough lines
in the buffer."
  (if (not (zerop (forward-line i)))
      (error "There aren't that many lines in the buffer!")))

(defun fortran-move-target (target &optional nlines)
  (save-excursion
    (goto-char target)
    (fortran-forward-line1 nlines)
    (point)))

(defun fortran-forward-line (nlines) "\
Move forward by NLINES fortran lines, handling include lines, form feeds, etc."
  (if (> nlines 0)
      (let (target linesleft)
	(save-excursion
	  (setq linesleft (forward-line nlines))
	  (setq target (point)))
	(while (re-search-forward fortran-special-regexp target t)
	  (if (= (char-after (match-beginning 0)) ?\C-l)
	      (if (= target (point-max))
		  (setq linesleft (1+ linesleft))
		(setq target (fortran-move-target target)))
	    (let ((region-lines (count-lines (point) target))
		  (include-file (buffer-match-substring 1))
		  (included-lines (fortran-count-include-lines
				   (buffer-match-substring 1))))
	      (if (< included-lines (+ region-lines linesleft))
		  (if (>= linesleft included-lines)
		      (setq linesleft (- linesleft included-lines))
		    (progn
		      (setq target (fortran-move-target
				    target
				    (- linesleft included-lines)))
		      (setq linesleft 0)))
		(progn
		  (switch-to-buffer (fortran-find-include-file include-file))
		  (goto-char (point-min))
		  (fortran-forward-line (+ region-lines linesleft -1))
		  (setq linesleft 0)
		  (setq target (point)))))
	))
	(goto-char target)
	(fortran-forward-line1 linesleft)
	(while (looking-at "^\C-l$")
	  (fortran-forward-line1)))))

	  
(defun fortran-goto-line (destline &optional routine) "\
*Moves point to the beginning of line DESTLINE in fortran routine ROUTINE"
  (interactive "NGoto line: ")
  (if (null routine)
      (if (interactive-p)
	  (setq routine (read-no-blanks-default
			 (concat "Goto line: " destline " in routine")
			 (fortran-find-routine-name)))
	(setq routine (fortran-find-routine-name))))
  (if (not (stringp routine))
      (error "The routine name must be a string!"))
  (let ((case-fold-search t))
    (goto-char (point-min))
    (if (not (re-search-forward (fortran-routine-regexp routine) nil t))
	(error "I can't find the routine `%s'" routine))
    (beginning-of-line)
    (re-search-backward fortran-end-regexp nil 'move)
    (if (looking-at fortran-end-regexp)
	(forward-line))
    (fortran-forward-line (1- destline))))

;;; auto-fill should call fortran-split-line to get continuation, etc.
;; guard against reassigning indent-new-comment-line twice
(if (not (boundp 'fortran-guard-variable))
    (progn
      (fset 'old-indent-new-comment-line
	    (symbol-function 'indent-new-comment-line))
      (setq fortran-guard-variable t)))

(defun indent-new-comment-line (&optional soft)
  "Break line at point and indent, continuing comment if presently within one.
The body of the continued comment is indented under the previous comment line.
Modified to work with fortran-mode."
  (interactive "*")
  (if (eq major-mode 'fortran-mode)
      (fortran-split-line soft)
    (old-indent-new-comment-line soft)))


;; these redefine things in fortran.el

;; (defun fortran-split-line (&optional soft)
;;   "Break line at point and insert continuation marker and alignment."
;;   (interactive)
;;   (delete-horizontal-space)
;;   (if (save-excursion (beginning-of-line) (looking-at comment-line-start-skip))
;;       ;; copy comment introducer from previous line
;;       (progn
;; 	(if soft (insert-and-inherit "\n") (newline 1))
;; 	(insert-and-inherit (buffer-match-substring 1) " "))
;;     (if fortran-tab-mode
;; 	(progn 
;; 	  (if soft (insert-and-inherit "\n") (newline 1))
;; 	  (insert-and-inherit "\t")
;; 	  (insert-char (fortran-numerical-continuation-char) 1 t))
;;       (if soft (insert-and-inherit "\n") (newline 1))
;;       (insert-and-inherit " " fortran-continuation-string)));Space after \n important
;;     (fortran-indent-line))		;when the cont string is C, c or *.

;; (defun calculate-fortran-indent ()
;;   "Calculates the fortran indent column based on previous lines."
;;   (let (icol first-statement (case-fold-search t))
;;     (save-excursion
;;       (setq first-statement (fortran-previous-statement))
;;       (if first-statement
;; 	  (setq icol fortran-minimum-statement-indent)
;; 	(progn
;; 	  (if (= (point) (point-min))
;; 	      (setq icol fortran-minimum-statement-indent)
;; 	    (setq icol (fortran-current-line-indentation)))
;; 	  (skip-chars-forward " \t0-9")
;; 	  (cond ((looking-at "if[ \t]*(")
;; 		 (if (or (looking-at ".*)[ \t]*then\\b[ \t]*[^ \t(=a-z0-9]")
;; 			 (let (then-test)	;multi-line if-then
;; 			   (while (and (= (forward-line 1) 0) ;search forward for then
;; 				       (looking-at "     [^ 0]")
;; 				       (not (setq then-test (looking-at ".*then\\b[ \t]*[^ \t(=a-z0-9]")))))
;; 			   then-test))
;; 		     (setq icol (+ icol fortran-if-indent))))
;; 		((looking-at "\\(else\\|elseif\\)\\b")
;; 		 (setq icol (+ icol fortran-if-indent)))
;; 		((looking-at "do\\b")
;; 		 (setq icol (+ icol fortran-do-indent)))))))
;;     (save-excursion
;;       (beginning-of-line)
;;       (cond ((looking-at "[ \t]*$"))
;; 	    ((looking-at comment-line-start-skip)
;; 	     (cond ((eq fortran-comment-indent-style 'relative)
;; 		    (setq icol (+ icol fortran-comment-line-column)))
;; 		   ((eq fortran-comment-indent-style 'fixed)
;; 		    (setq icol fortran-comment-line-column))
;; 		   ((eq fortran-comment-indent-style 'previous)
;; 		    (let (moved)
;; 		      (save-excursion
;; 			(while (and (setq moved (zerop (forward-line -1)))
;; 				    (looking-at "[ \t]*$")))
;; 			(if (and moved
;; 				 (looking-at (concat comment-line-start-skip
;; 						     "[^ \n\t]+")))
;; 			    (setq icol (fortran-current-line-indentation))
;; 			  (setq icol fortran-comment-line-column))))) ))
;; 	    ((or (looking-at (concat "[ \t]*"
;; 				     (regexp-quote
;; 				      fortran-continuation-string)))
;; 		 (looking-at "     [^ 0\n]"))
;; 	     (setq icol (+ icol fortran-continuation-indent))
;; 	     (let (moved)
;; 	       (while
;; 		   (and (setq moved (zerop (forward-line -1)))
;; 			(or (looking-at comment-line-start-skip)
;; 			    (looking-at "[ \t]*$")
;; 			    (looking-at (concat "[ \t]*"
;; 						comment-start-skip)))))
;; 	       (if moved
;; 		   (cond
;; 		    ((or (looking-at
;; 			  (concat "[ \t]*"
;; 				  (regexp-quote
;; 				    fortran-continuation-string)))
;; 			 (looking-at "     [^ 0\n]"))
;; 		     (setq icol (fortran-current-line-indentation)))
;; 		    ((save-excursion
;; 		       (save-restriction
;; 			 (narrow-to-region (point)
;; 					   (save-excursion
;; 					     (end-of-line)
;; 					     (point)))
;; 			 (condition-case nil
;; 			     (progn
;; 			       (while (progn
;; 					(skip-chars-forward "^(")
;; 					(and (not (eobp))
;; 					     (= (char-after (point)) 40)))
;; 				 (forward-list))
;; 			       nil)
;; 			   (error
;; 			    (setq icol (1+ (current-column)))
;; 			    t)))))
;; 		    ((looking-at "[ \t]*[0-9]*[ \t]*if[ \t]*(")
;; 		     (setq icol (+ (- icol fortran-continuation-indent)
;; 				   fortran-if-indent)))
;; 		     ))))
;; 	    (first-statement)
;; 	    ((and fortran-check-all-num-for-matching-do
;; 		  (looking-at "[ \t]*[0-9]+")
;; 		  (fortran-check-for-matching-do))
;; 	     (setq icol (- icol fortran-do-indent)))
;; 	    (t
;; 	     (skip-chars-forward " \t0-9")
;; 	     (cond ((looking-at "end[ \t]*if\\b")
;; 		    (setq icol (- icol fortran-if-indent)))
;; 		   ((looking-at "\\(else\\|elseif\\)\\b")
;; 		    (setq icol (- icol fortran-if-indent)))
;; 		   ((and (looking-at "continue\\b")
;; 			 (fortran-check-for-matching-do))
;; 		    (setq icol (- icol fortran-do-indent)))
;; 		   ((looking-at "end[ \t]*do\\b")
;; 		    (setq icol (- icol fortran-do-indent)))
;; 		   ((and (looking-at "end\\b[ \t]*[^ \t=(a-z]")
;; 			 (not (= icol fortran-minimum-statement-indent)))
;;  		    (message "Warning: `end' not in column %d.  Probably an unclosed block." fortran-minimum-statement-indent)))))
;;       (beginning-of-line)
;;       (if (looking-at comment-line-start-skip)
;; 	  icol
;; 	(max fortran-minimum-statement-indent icol)))))

(setq fortran-mode-hook '(lambda nil
  (setq fortran-startup-message nil)
  (setq comment-start "!")
  (setq fortran-continuation-string "&")
  (setq fortran-do-indent 2)
  (setq fortran-if-indent 2)
  (setq fill-column 72)
  ;; add parens so we can pick out the comment introducer
  (setq comment-line-start-skip "^\\([Cc*]\\( \\*+\\|[^ \t\n]*\\)\\)[ \t]*")
  (modify-syntax-entry ?$ "w" fortran-mode-syntax-table)
;;  (modify-syntax-entry ?_ "w" fortran-mode-syntax-table)
  (define-abbrev fortran-mode-abbrev-table ";en" "endif" 'fortran-indent-line)
  (define-abbrev fortran-mode-abbrev-table ";e" "else" 'fortran-indent-line)
  (define-abbrev fortran-mode-abbrev-table ";el" "elseif" 'fortran-indent-line)
  (define-abbrev fortran-mode-abbrev-table ";ed" "enddo" 'fortran-indent-line)
  (define-abbrev fortran-mode-abbrev-table "endif" "endif"
    'fortran-indent-line)
  (define-abbrev fortran-mode-abbrev-table "else" "else"
    'fortran-indent-line)
  (define-abbrev fortran-mode-abbrev-table "elseif" "elseif"
    'fortran-indent-line)
  (define-abbrev fortran-mode-abbrev-table "enddo" "enddo"
    'fortran-indent-line)
  (define-abbrev fortran-mode-abbrev-table ";fu" "function"
    'D0-function-header)
  (define-abbrev fortran-mode-abbrev-table ";pr" "program" 'D0-program-header)
  (define-abbrev fortran-mode-abbrev-table ";su" "subroutine"
    'D0-subroutine-header)

;;  (define-key fortran-mode-map "\C-c?" 'fortran-what-line)
;;  (define-key fortran-mode-map "\C-cl" 'fortran-goto-line)
  (define-key fortran-mode-map "\C-ci" 'fortran-follow-include)
  (define-key fortran-mode-map "\C-c4i" 'fortran-follow-include-other-window)
  (abbrev-mode 1)
  (auto-fill-mode 1)
  ))

(setq auto-mode-alist
      (cons '("\\.inc$" . fortran-mode) auto-mode-alist))
(setq auto-mode-alist
      (cons '("\\.for$" . fortran-mode) auto-mode-alist))
(setq auto-mode-alist
      (cons '("\\.zebfor$" . fortran-mode) auto-mode-alist))
(setq auto-mode-alist
      (cons '("\\.zebinc$" . fortran-mode) auto-mode-alist))

(setq completion-ignored-extensions
      (cons ".zebpp" completion-ignored-extensions))


;;(if (boundp 'function-keymap)
;;    (define-key function-keymap "f" 'D0-find-function))


;;;;;;;;;;;;;
(setq fortran-minimum-statement-indent 6)
(setq fortran-comment-line-column 0)
(setq fortran-tab-mode nil)

