(require 'add-log)
(require 'cl) ; for dolist
(require 'vc)


(defun cvs-log-buffer-match-substring (&optional i) "\
Returns the text in the current buffer matched by subexp I of last search"
  (if (null i) (setq i 0))
  (buffer-substring
   (match-beginning i)
   (match-end i)))



;; Get the absolute path of the directory to which the commit is relative.
(defun cvs-log-get-root ()
  (save-excursion
    (let (curdir commitdir)
      (goto-char (point-min))
      (if (re-search-forward "CVS: Current directory: \\(\\S-*\\)"
                             (point-max) t)
          (setq curdir (file-truename (cvs-log-buffer-match-substring 1))))
      (goto-char (point-min))
      (if (re-search-forward "CVS: Committing in \\(\\S-*\\)"
                             (point-max) t)
          (setq commitdir (cvs-log-buffer-match-substring 1)))
      (if (and curdir commitdir)
          (file-truename commitdir curdir)
        nil))))

           
;; Get a list of all files involved in this commit.
(defun cvs-log-get-files (root)
  (save-excursion
    (let (files)
      (goto-char (point-min))
      (re-search-forward "CVS: Committing in \\(\\S-*\\)")
      (forward-line 1)
      (while (re-search-forward "^CVS: \t.*$" (point-max) t)
        (beginning-of-line)
        (setq files (append files (cvs-log-get-files-from-line root)))
        (forward-line 1))
      files)))


(defun cvs-log-get-files-from-line (root)
  (save-excursion
    (forward-char 4)  ; skip `CVS:'
    (skip-chars-forward " \t")
    (let (files)
      (while (not (eolp))
        (let ((beg (point)))
          (skip-chars-forward "^ \t\n")
          (setq files
                (append files (list (file-truename
                                     (buffer-substring beg (point))
                                     root)))))
        (skip-chars-forward " \t"))
      files)))


;; Return the change log for FILE.
;; Does a find-file on FILE.
(defun cvs-log-changelog-for-file (file)
  (save-excursion

    ;; If the file no longer exists, try to find another file in the same
    ;; directory.  If there aren't any other files, go up the hierarchy.
    ;; XXX goes into an infinite loop if the directory is empty!
    ;; XXX need to do this with out the file-file anyway...
    (when (not (file-readable-p file))
      (let ((l nil))
        (while (not l)
          (setq file (file-name-directory file))
          (setq l (directory-files file t nil nil t)))
        (setq file (car l))))

    (set-buffer (find-file-noselect file))
    (file-truename (find-change-log))))


;; Make an alist ((LOG FILE...) (LOG FILE...) ...)
(defun cvs-log-find-changelogs (files)
  (let (loglist)
    (dolist (f files)
      (let* ((cl (cvs-log-changelog-for-file f))
             (flist (assoc cl loglist)))
        (if flist
            (nconc flist (list f))
          (setq loglist (cons (list cl f) loglist)))))
    loglist))


(defun cvs-log-doit ()
  (interactive)
  (setq root (cvs-log-get-root))
  (setq files (cvs-log-get-files root))
  (setq cl (cvs-log-find-changelogs files))
  (setq changelist nil)
  (dolist (c cl)
    (let ((elt (list (car c))))
      (cvs-log-process-changelog elt)
      (setq changelist (cons elt changelist))))
  (setq changelist (nreverse changelist))
  (cvs-log-insert-from-changelogs cl changelist)
  (let ((end (point)))
    (forward-line -1)
    (delete-region (point) end)))


;; Process changes in a changelog.
;; ALIST should be a single element list containing the
;; changelog name.
;; On output, the changes will be added to the end of the list:
;;
;;   (NAME (HEADER1 CHANGE1.1 CHANGE1.2...)  (HEADER2 ...) ...)
;;
(defun cvs-log-process-changelog (alist)
  (let ((clfile (car alist)))
    (save-excursion
      (set-buffer (cvs-log-get-changelog-diffs clfile))
      (goto-char (point-min))
      (while (re-search-forward "^@@ .*\\+\\([0-9]+\\)," (point-max) t)
        (forward-line 1)
        (let ((srcline (string-to-int (cvs-log-buffer-match-substring 1)))
              (beg (point)))
          (re-search-forward "^@@" (point-max) 'move)
          (beginning-of-line)
          (cvs-log-process-changelog-hunk clfile srcline beg (point) alist)))))
  alist)


;; process one hunk of changes.
;; Results are added to ALIST.
(defun cvs-log-process-changelog-hunk (clfile srcline beg end alist)
  (save-excursion
    (goto-char beg)
    (while (re-search-forward "^\\+[ \t]+\\*" end t)
      (beginning-of-line)
      (let ((chbeg (point)))
        (while (looking-at "^\\+[ \t]+[^ \t\n]+")
          (forward-line 1))
        (cvs-log-process-changelog-change clfile
                                          (+ srcline (count-lines beg chbeg))
                                          chbeg (point) alist))))
  alist)


;; process a single change.
;; Results are added to ALIST.
(defun cvs-log-process-changelog-change (clfile srcline beg end alist)
  (save-excursion
    (let ((change "") header)
      (goto-char beg)
      (while (< (point) end)
        (forward-char 1)
        (let ((lbeg (point)))
          (end-of-line)
          (forward-char 1)
          (setq change (concat change (buffer-substring lbeg (point))))))
      (setq header (cvs-log-find-change-header clfile srcline change))
      (cvs-log-add-change-to-list header change alist)))
  alist)


(defun cvs-log-add-change-to-list (header change alist)
  (let ((elt (assoc header (cdr alist))))

    ;; Add the alist elt if it's not already there.
    (if (not elt)
        (nconc alist (list (setq elt (list header)))))

    (nconc elt (list change))))


;; Find the header for change CHANGE in changelog CLFILE around line SRCLINE.
(defun cvs-log-find-change-header (clfile srcline change)
  (save-excursion
    (set-buffer (find-file-noselect clfile))
    (goto-line srcline)
    (if
        (re-search-backward
         "^\\([0-9][0-9][0-9][0-9]-\\|Sun\\|Mon\\|Tue\\|Wed\\|Thu\\|Fri\\|Sat\\)" 
         (point-min) t)
        (let (beg)
          (beginning-of-line)
          (setq beg (point))
          (end-of-line)
          (buffer-substring beg (point)))
      "")))


;; return a buffer containing the diffs for CL.
(defun cvs-log-get-changelog-diffs (clfile)
  (save-excursion
    (save-window-excursion
      (let ((diff-switches "-u")
            (buf (generate-new-buffer "*cvs-log*"))
            diffbuf)
        (set-buffer (find-file-noselect clfile))
        (if (vc-diff nil)
            (progn
              (setq diffbuf (get-buffer "*vc-diff*"))
              (if (not diffbuf)
                  (setq diffbuf (get-buffer "*vc*")))
              (set-buffer diffbuf)
              (copy-to-buffer buf (point-min) (point-max))))
        buf))))



;; Insert into the current buffer any changes from HUNK relevant to FILES.
;; HUNK has the form (HEADER CHANGE CHANGE CHANGE ...).
;; FILES is a list of file names.
(defun cvs-log-insert-from-hunk (hunk files)
  (let ((beg (point)) inserted-any)
    (dolist (change (cdr hunk))
      (dolist (f files)
        (when (string-match (regexp-quote (file-name-nondirectory f)) change)
          (insert change)
          (insert "\n")
          (setq inserted-any t)
          (return))))

    ;; If we inserted any changes, add in the header too.
    (if inserted-any
        (save-excursion
          (goto-char beg)
          (insert (car hunk))
          (insert "\n\n")))))


;; Insert into the current buffer any changes from HUNKS relevant to FILES.
;; HUNKS is a list of hunks:
;;   ((HEADER CHANGE CHANGE ...)  (HEADER CHANGE CHANGE ...) ...)
;; FILES is a list of file names.
(defun cvs-log-insert-from-hunks (hunks files)
  (dolist (h hunks)
    (cvs-log-insert-from-hunk h files)))



(defun cvs-log-insert-from-changelogs (changelogs changelist)
  (dolist (c cl)
    (let* ((clname (car c))
           (files (cdr c))
           (changeelt (assoc clname changelist)))
      (if changeelt
          (cvs-log-insert-from-hunks (cdr changeelt) files)))))

