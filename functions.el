;;;
;;; small helper functions.
;;;

(defun term-xon nil "Turn on flow control on the HDS terminal"
  (send-string-to-terminal "\e[1;1;0;0/r"))

(defun term-xoff nil "Turn off flow control on the HDS terminal"
  (send-string-to-terminal "\e[0;0;0;0/r"))


(fset 'string-lessp* 'string<)


(defun bump-register (reg)
  (interactive "cBump register:")
  (let ((old (get-register reg)))
    (set-register reg
		  (format (format "%%00%dd" (length old))
			  (1+ (string-to-int old))))))


;;;
;;; Utility to test if a given extension file is available.
;;;


(if (fboundp 'locate-file)
    (defun library-exists (name)
      (locate-file name load-path '(".el" ".elc")))
  (require 'find-file)
  (defun library-exists (name)
    (ff-get-file-name load-path name '(".el" ".elc"))))


(defun maybe-require (sym)
  (if (library-exists (symbol-name sym))
      (require sym)))


;;;
;;; Extensions for python-mode.
;;;
(defun py-kill-into-nomenclature (&optional arg)
  (interactive "p")
  (let ((beg (point)))
    (py-forward-into-nomenclature arg)
    (kill-region beg (point))))


;;;
;;; Missing in earlier emacs versions.
;;;
(require 'custom)
(if (not (fboundp 'custom-quote))
    (defun custom-quote (sexp)
      "Quote SEXP if it is not self quoting."
      (if (or (memq sexp '(t nil))
              (keywordp sexp)
              (and (listp sexp)
                   (memq (car sexp) '(lambda)))
              (stringp sexp)
              (numberp sexp)
              (vectorp sexp)
              )
          sexp
        (list 'quote sexp)))
)