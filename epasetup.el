;;;
;;; epasetup.el
;;;
;;; configuration for the epa (easy gpg package)
;;;


; epa is enabled by default in emacs 23.
(maybe-require 'epa-setup) ; this does it for xemacs
(when (fboundp 'epa-file-enable)
  (epa-file-enable)

  ;;
  ;; I want to write files with ascii armor.
  ;; Actually, it would be better to remember if the file was
  ;; armored when it was read, and write it back the same way.
  ;; But just do this for now.
  ;;
  (defadvice epa-file-write-region (around use-armor)
    "Force saving gpg files with ascii armor."
    (let ((epa-armor t)) ad-do-it))
  (ad-activate 'epa-file-write-region)


  ;; Enable caching of the symmetric encryption key.
  (setq epa-file-cache-passphrase-for-symmetric-encryption t)


  ;; But delete the key when the buffer is deleted.
  (require 'cl)
  (defun epa-buffer-kill-hook ()
    (setq epa-file-passphrase-alist
          (delete-if (lambda (c)
                       (equal (car c) (buffer-file-name (current-buffer))))
                     epa-file-passphrase-alist)))
  (add-hook 'kill-buffer-hook 'epa-buffer-kill-hook)
)

